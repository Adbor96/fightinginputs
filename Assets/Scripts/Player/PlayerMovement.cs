﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum playerStates { idle, moving, jumping, wallGlide, damaged, attackingOnGround, attackingInJump, attackingOnWall}
public class PlayerMovement : MonoBehaviour
{
    private Rigidbody2D myRb;
    private float x;
    private float y;
    [SerializeField] private float speed;
    [SerializeField] private float groundCheckDistance;
    [SerializeField] private float wallCheckDistance;
    [SerializeField] private LayerMask groundMask;
    [SerializeField] private LayerMask wallMask;
    [SerializeField] private float wallSlideSpeed;
    [SerializeField] private float xWallForce;
    [SerializeField] private float yWallForce;
    private float defaultGravity;

    [SerializeField] private float jumpForce;
    private bool isJumping;

    private playerStates currentState;

    private bool playerFacingRight;

    private Vector2 wallSearchDirection;

    private Animator myAnim;

    private CircleCollider2D myCollider;
    // Start is called before the first frame update
    void Start()
    {
        myRb = GetComponent<Rigidbody2D>();
        currentState = playerStates.idle;
        defaultGravity = myRb.gravityScale;
        playerFacingRight = true;
        myAnim = transform.GetChild(1).GetComponent<Animator>();
        myCollider = GetComponent<CircleCollider2D>();

    }

    // Update is called once per frame
    void Update()
    {
        x = Input.GetAxisRaw("Horizontal");
        y = Input.GetAxisRaw("Vertical");

        if (myRb.velocity.x == 0 && IsGrounded())
            currentState = playerStates.idle;

        if (myRb.velocity.x != 0 && IsGrounded())
            currentState = playerStates.moving;
        if (myRb.velocity.y > 0)
            currentState = playerStates.jumping;
        Flip();


        if (Input.GetButtonDown("Fire2") && IsGrounded())
        {
            isJumping = true;
        }
        if (playerFacingRight)
        {
            wallSearchDirection = Vector2.right;
        }
        else
        {
            wallSearchDirection = Vector2.left;
        }

        if(currentState != playerStates.wallGlide)
        {
            myRb.gravityScale = defaultGravity;
        }
        switch (currentState)
        {
            case playerStates.idle:
                myAnim.SetBool("Moving", false);
                break;

            case playerStates.moving:
                //player is moving
                myAnim.SetBool("Moving", true);
                break;

            case playerStates.jumping:
                if (CanWallGlide() && !IsGrounded() && x != 0)
                {
                    currentState = playerStates.wallGlide;
                }
                break;

            case playerStates.wallGlide:
                myRb.velocity = new Vector2(myRb.velocity.x, Mathf.Clamp(myRb.velocity.y, -wallSlideSpeed, float.MaxValue));
                if(Input.GetButtonDown("Fire2"))
                {
                    WallJump();
                }
                if (IsGrounded())
                {
                    if(x != 0)
                    {
                        currentState = playerStates.moving;
                    }
                    else
                    {
                        currentState = playerStates.idle;
                    }
                }
                if (!CanWallGlide())
                {
                    if (!IsGrounded())
                    {
                        currentState = playerStates.jumping;
                    }
                    else
                    {
                        currentState = playerStates.jumping;
                    }
                }
                break;
            case playerStates.attackingOnGround:
                break;
        }
    }
    private void FixedUpdate()
    {
        myRb.velocity += Vector2.right * x * speed * Time.deltaTime;
        if (isJumping && IsGrounded())
        {
            Jump();
            myAnim.SetBool("Jumping", true);
        }
    }
    void Jump()
    {
        myRb.velocity += Vector2.up * jumpForce;
        if (IsGrounded())
        {
            isJumping = false;
        }
    }

    bool CanWallGlide()
    {
        Debug.DrawRay(transform.position, wallSearchDirection * wallCheckDistance, Color.red);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, wallSearchDirection, wallCheckDistance, wallMask);
        if (hit.collider != null)
        {
            //Debug.Log("Wall");
            return true;
        }
        //Debug.Log(hit.collider);
        return false;
    }
    bool IsGrounded()
    {
        /*Debug.DrawRay(transform.position, Vector2.down * groundCheckDistance, Color.red);
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, groundCheckDistance, groundMask);
        if(hit.collider != null)
        {
            myAnim.SetBool("Jumping", false);
            return true;
        }
        //Debug.Log(hit.collider);
        return false;*/

        RaycastHit2D raycastHit = Physics2D.Raycast(myCollider.bounds.center, Vector2.down, myCollider.bounds.extents.y + groundCheckDistance, groundMask);
        Color rayColor;
        if(raycastHit.collider != null)
        {
            rayColor = Color.green;
            myAnim.SetBool("Jumping", false);
        }
        else
        {
            rayColor = Color.red;
        }
        Debug.DrawRay(myCollider.bounds.center, Vector2.down * (myCollider.bounds.extents.y + groundCheckDistance), rayColor);
        return raycastHit.collider != null;
    }
    void Flip()
    {
        if(x > 0)
        {
            if (!playerFacingRight)
            {
                playerFacingRight = true;
                transform.localScale = new Vector3(1, 1, 1);
            }
        }
        else if (x < 0)
        {
            if (playerFacingRight)
            {
                playerFacingRight = false;
                transform.localScale = new Vector3(-1, 1, 1);
            }
        }
    }
    void WallJump()
    {
        myRb.velocity = new Vector2(xWallForce * -x, yWallForce);
    }
}
