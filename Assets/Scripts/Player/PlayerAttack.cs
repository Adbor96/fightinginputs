﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField] private int damage;
    private PlayerMovement movement;
    private Transform attackPoint;
    [SerializeField] private float attackRadius;
    [SerializeField] private float meleeRate;
    private float nextAttackTime;
    private playerStates currentState;
    private Animator myAnim;
    // Start is called before the first frame update
    void Start()
    {
        attackPoint = transform.GetChild(0).transform;
        movement = GetComponent<PlayerMovement>();
        myAnim = transform.GetChild(1).GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1") && Time.time >= nextAttackTime)
        {
            switch (currentState)
            {
                case playerStates.idle:
                    currentState = playerStates.attackingOnGround;
                    Attack();
                    currentState = playerStates.idle;
                    break;

                case playerStates.moving:
                    Attack();
                    break;

                case playerStates.jumping:
                    currentState = playerStates.attackingInJump;
                    Attack();
                    currentState = playerStates.jumping;
                    break;

                case playerStates.wallGlide:
                    currentState = playerStates.attackingOnWall;
                    Attack();
                    break;
            }

        }
    }
    void Attack()
    {
        myAnim.SetTrigger("Attack");
        Collider2D[] enemiesHit = Physics2D.OverlapCircleAll(attackPoint.position, attackRadius);
        foreach (Collider2D enemy in enemiesHit)
        {
            IDamageable damageable = enemy.GetComponent<IDamageable>();
            if(damageable != null)
            {
                damageable.TakeDamage(damage);
            }
        }
        nextAttackTime = Time.time + 1f / meleeRate;
    }
    private void OnDrawGizmos()
    {
        //Gizmos.DrawWireSphere(attackPoint.position, attackRadius);
    }
}
