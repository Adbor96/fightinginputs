﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hadouken : MonoBehaviour
{
    private Rigidbody2D myRb;
    [SerializeField] private float speed;
    private Vector2 playerPosition;
    private Vector2 direction;
    // Start is called before the first frame update
    void Start()
    {
        myRb = GetComponent<Rigidbody2D>();
        Vector2 directionX = ((Vector3)playerPosition - transform.position);
        direction = new Vector2(directionX.x, 0).normalized;

        if (playerPosition.x <= transform.position.x)
        {
            transform.localScale = new Vector2(1, -1);
        }
    }

    // Update is called once per frame
    void Update()
    {
        myRb.velocity = direction * speed;
    }
    public void Setup(Vector2 _playerPosition)
    {
        playerPosition = _playerPosition;
    }
}
