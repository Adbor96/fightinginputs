﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Special Moves/PlayerHadouken")]
public class PlayerHadouken : QuarterCircleForward
{
    public playerStates currentState;
    [SerializeField] private Hadouken projectile;

    public override void Execute(GameObject player)
    {

        switch (currentState)
        {
            case playerStates.idle:

                SpawnProjectile(player);
                break;

            case playerStates.moving:

                SpawnProjectile(player);
                break;
        }
    }
    void SpawnProjectile(GameObject _player)
    {
        Transform projectileExit;
        projectileExit = _player.transform.GetChild(2).transform;
        Hadouken projectileSpawn = Instantiate(projectile, projectileExit.position, Quaternion.identity);
        projectile.Setup(_player.transform.position);
    }
}
