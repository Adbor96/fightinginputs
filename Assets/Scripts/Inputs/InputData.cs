﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[System.Serializable]
public class InputData
{
    public Inputs _input; //den riktning spaken är i och vilken knapp som tryckts
    public float inputTime; //vilken tidpunkt ett input registrerades, används för att se om spelaren tagit för lång tid att göra ett special move
    public bool canContinueMove;
    public InputData(Inputs typeOfInput)
    {
        _input = typeOfInput;
        inputTime = Time.time;
    } 
}
