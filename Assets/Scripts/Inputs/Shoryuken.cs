﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Special Moves/Shoryuken")]
public class Shoryuken : SpecialMoves
{

    private void Awake()
    {

    }
    public override bool CheckActivation(List<InputData> lastInputs)
    {
        if (lastInputs.Count >= 5)
        {
            if (lastInputs[lastInputs.Count - 1]._input == Inputs.A)
            {
                    if (lastInputs[lastInputs.Count - 2]._input == Inputs.D3 && lastInputs[lastInputs.Count - 2].inputTime >= lastInputs[lastInputs.Count - 2].inputTime - 1)
                    {
                        if (lastInputs[lastInputs.Count - 3]._input == Inputs.D2 && lastInputs[lastInputs.Count - 3].inputTime >= lastInputs[lastInputs.Count - 3].inputTime - 1)
                        {
                            if (lastInputs[lastInputs.Count - 4]._input == Inputs.D5 && lastInputs[lastInputs.Count - 4].inputTime >= lastInputs[lastInputs.Count - 4].inputTime - 1)
                            {
                                //canDoAnotherMove = false;
                                if (lastInputs[lastInputs.Count - 5]._input == Inputs.D6 && lastInputs[lastInputs.Count - 5].inputTime >= lastInputs[lastInputs.Count - 5].inputTime - 1)
                                {
                                    return true;
                                }
                            }
                        }
                    }
            }
        }
        //canDoAnotherMove = true;
        return false;
    }
}
