﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Special Moves/Hadouken")]
public class QuarterCircleForward : SpecialMoves
{


    public override bool CheckActivation(List<InputData> lastInputs)
    {
        if (lastInputs.Count >= 4) //om det finns minst 4 inputs registrerade
        {
            if (lastInputs[lastInputs.Count - 1]._input == Inputs.A) //om man trycka Fire1 efter att ha gjort en kvartcirkel snabbt nog
            {
                if (lastInputs[lastInputs.Count - 2]._input == Inputs.D6 && lastInputs[lastInputs.Count - 2].inputTime >= lastInputs[lastInputs.Count - 1].inputTime - 1)
                {
                    if (lastInputs[lastInputs.Count - 3]._input == Inputs.D3 && lastInputs[lastInputs.Count - 3].inputTime >= lastInputs[lastInputs.Count - 2].inputTime - 1)
                    {
                        if (lastInputs[lastInputs.Count - 4]._input == Inputs.D2 && lastInputs[lastInputs.Count - 4].inputTime >= lastInputs[lastInputs.Count - 3].inputTime - 1)
                        {
                            return true; //är det sant
                        }
                    }
                }
            }
        }
        return false;
    }
}
