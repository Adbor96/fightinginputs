﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Special Moves/Backwards")]
public class QuarterCircleBackWard : SpecialMoves
{

    public override bool CheckActivation(List<InputData> lastInputs)
    {
        if (lastInputs.Count >= 4)
        {
            if (lastInputs[lastInputs.Count - 1]._input == Inputs.B)
            {
                if (lastInputs[lastInputs.Count - 2]._input == Inputs.D4 && lastInputs[lastInputs.Count - 2].inputTime >= lastInputs[lastInputs.Count - 1].inputTime - 1)
                {
                    if (lastInputs[lastInputs.Count - 3]._input == Inputs.D1 && lastInputs[lastInputs.Count - 3].inputTime >= lastInputs[lastInputs.Count - 2].inputTime - 1)
                    {
                        if (lastInputs[lastInputs.Count - 4]._input == Inputs.D2 && lastInputs[lastInputs.Count - 4].inputTime >= lastInputs[lastInputs.Count - 3].inputTime - 1)
                        {
                                return true;
                        }
                    }
                }
            }
        }
        return false;
    }
}
