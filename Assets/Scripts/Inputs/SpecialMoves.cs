﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialMoves : ScriptableObject
{
    //public bool canDoAnotherMove = true;
    //protected List<Inputs> _lastInputs;
    public virtual bool CheckActivation(List<InputData> data) //kolla om man har aktiverat ett special move
    {
        return false;
    }
    public virtual void Execute(GameObject player)
    {
        Debug.ClearDeveloperConsole();
        Debug.Log(this.name);
    }
}
