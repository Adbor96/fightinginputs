﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum Inputs
{
    D1, D2, D3, D4, D5, D6, D7, D8, D9, A, B, X, Y, NULL
}
public class StickInput : MonoBehaviour
{
    public List<InputData> lastInputs; //de senaste riktningarna
    public List<SpecialMoves> specialMoves;
    // Start is called before the first frame update
    void Start()
    {
        lastInputs = new List<InputData>();
        lastInputs.Add(StickDirection());
    }

    // Update is called once per frame
    void Update()
    {
        if (lastInputs[lastInputs.Count - 1]._input != StickDirection()._input)
        {
            lastInputs.Add(StickDirection());
        }

        if(PressedButton() != null)
        {
            lastInputs.Add(PressedButton());
        }

        for (int i = 0; i < specialMoves.Count; i++) //gå igenom alla special moves karaktären har
        {
            //kolla om någon av de genomförs
            if (specialMoves[i].CheckActivation(lastInputs))
            {
                specialMoves[i].Execute(gameObject);
            }
        }
    }
    public InputData StickDirection()
    {
        float xAxis = Input.GetAxis("Horizontal");
        float yAxis = Input.GetAxis("Vertical");

        if (xAxis <= -0.3f && yAxis <= -0.3f)
        {
            return new InputData(Inputs.D1);
        }
        if (xAxis <= 0.3f && xAxis >= -0.3f && yAxis <= -0.3f)
        {
            return new InputData(Inputs.D2);
        }
        if (xAxis >= 0.3f && yAxis <= -0.3f)
        {
            return new InputData(Inputs.D3);
        }
        if (xAxis <= -0.3f && yAxis <= 0.3f && yAxis >= -0.3f)
        {
            return new InputData(Inputs.D4);
        }
        if (xAxis >= -0.3f && xAxis <= 0.3f && yAxis <= 0.3f && yAxis >= -0.3f)
        {
            return new InputData(Inputs.D5);
        }
        if (xAxis >= 0.3f && yAxis >= -0.3f && yAxis <= 0.3f)
        {
            return new InputData(Inputs.D6);
        }
        if (xAxis <= -0.3f && yAxis >= 0.3f)
        {
            return new InputData(Inputs.D7);
        }
        if (xAxis <= 0.3f && xAxis >= -0.3f && yAxis >= -0.3f)
        {
            return new InputData(Inputs.D8);
        }
        if (xAxis >= 0.3f && yAxis >= 0.3f)
        {
            return new InputData(Inputs.D9);
        }
        return null;
    }
    public InputData PressedButton()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            return new InputData(Inputs.A);
        }
        if (Input.GetButtonDown("Fire2"))
        {
            return new InputData(Inputs.B);
        }
        if (Input.GetButtonDown("Fire3"))
        {
            return new InputData(Inputs.X);
        }
        if (Input.GetButtonDown("Fire4"))
        {
            return new InputData(Inputs.Y);
        }
        return null;
    }
}





