﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour, IDamageable
{
    [SerializeField] private int health;
    [SerializeField] private int damage;
    public void TakeDamage(int damageAmount)
    {
        Debug.Log("Health before damage " + health);
        health -= damageAmount;
        Debug.Log("Health after damage " + health);
        if(health <= 0)
        {

        }

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Die()
    {
        Destroy(gameObject);
    }
}
